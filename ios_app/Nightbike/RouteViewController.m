//
//  RouteViewController.m
//  Nightbike
//
//  Created by Aleksandar Palic on 08.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "RouteViewController.h"
#import "SearchViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import <CoreLocation/CoreLocation.h>

@interface RouteViewController () {
    CLLocationManager *locationManager;
    const NSString *apiKey;
    NSString *futureStartingAddress;
    NSString *futureDestinationAddress;
}
@end

@implementation RouteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // alloc & init attributes and properties
    locationManager = [[CLLocationManager alloc] init];
    apiKey = @"AIzaSyCbwxinDbmCv-HgvvJudgDmJxwK1IdIxsk";
    
    [self setStartingLocation:nil];
    [self setDestinationLocation:nil];
    
    // get current location
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation]; // line 47
}

- (void)viewWillAppear:(BOOL)animated
{
    //NSLog(@"%@", [self startingLocation]);
    //NSLog(@"%@", [self destinationLocation]);
    
    // check for enabling route button
    if ([self startingLocation] != nil && [self destinationLocation] != nil) {
        [[self routeButton] setEnabled:YES];
        [[self routeButton] setTitleColor:[UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
    }
    else {
        [[self routeButton] setEnabled:NO];
        [[self routeButton] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - CoreLocation Stuff

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    // starting location found, so stop updating
    [locationManager stopUpdatingLocation];
    
    // set starting location property
    [self setStartingLocation:@{
        @"lat": [NSString stringWithFormat:@"%f", newLocation.coordinate.latitude],
        @"lng": [NSString stringWithFormat:@"%f", newLocation.coordinate.longitude]
    }];
    
    if ([[[self startingLocationTextField] text] isEqualToString:@""]) {
        [[self startingLocationTextField] setText:@"Ermittle ..."];
        
        // call google and pass lat/lng to get the current location address
        NSString *googleUrl = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true", newLocation.coordinate.latitude, newLocation.coordinate.longitude];
        
        AFHTTPRequestOperationManager *httpmanager = [AFHTTPRequestOperationManager manager];
        [httpmanager GET:googleUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSArray *components = [[responseObject valueForKey:@"results"][0] valueForKey:@"address_components"];
            futureStartingAddress = [NSString stringWithFormat:@"%@ %@", [components[1] valueForKey:@"long_name"], [components[0] valueForKey:@"long_name"]];
            
            [[self startingLocationTextField] setText:[[responseObject valueForKey:@"results"][0] valueForKey:@"formatted_address"]];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }
}

#pragma mark - Button Actions

- (IBAction)startingLocationAction:(id)sender {
    UIStoryboard *storyBoard = [self storyboard];
    
    SearchViewController *svc = [storyBoard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    [svc setIsStartingLocation:YES];
    [svc setFutureSearchBarText:futureStartingAddress];
    
    [svc setDelegate:self];
    
    [self presentViewController:svc animated:YES completion:nil];
}

- (IBAction)destinationLocationAction:(id)sender {
    UIStoryboard *storyBoard = [self storyboard];
    
    SearchViewController *svc = [storyBoard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    [svc setIsStartingLocation:NO];
    [svc setFutureSearchBarText:futureDestinationAddress];
    
    [svc setDelegate:self];
    
    [self presentViewController:svc animated:YES completion:nil];
}

#pragma mark custom modal delegate

- (void)didDismissSearchViewController:(NSDictionary *)dictionary
{
    if ([[dictionary valueForKey:@"isStartingLocation"] boolValue]) {
        futureStartingAddress = [dictionary valueForKey:@"address"];
        [self setStartingLocation:[dictionary valueForKey:@"location"]];
        [[self startingLocationTextField] setText:futureStartingAddress];
    }
    else {
        futureDestinationAddress = [dictionary valueForKey:@"address"];
        [self setDestinationLocation:[dictionary valueForKey:@"location"]];
        [[self destinationLocationTextField] setText:futureDestinationAddress];
    }
}

@end
