//
//  RouteViewController.h
//  Nightbike
//
//  Created by Aleksandar Palic on 08.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "ViewController.h"
#import "SearchViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface RouteViewController : ViewController <CLLocationManagerDelegate, SearchViewControllerDelegate>

@property (nonatomic, strong) NSDictionary *startingLocation;
@property (nonatomic, strong) NSDictionary *destinationLocation;

@property (weak, nonatomic) IBOutlet UITextField *startingLocationTextField;
@property (weak, nonatomic) IBOutlet UITextField *destinationLocationTextField;
@property (weak, nonatomic) IBOutlet UIButton *routeButton;

- (IBAction)startingLocationAction:(id)sender;
- (IBAction)destinationLocationAction:(id)sender;

@end
