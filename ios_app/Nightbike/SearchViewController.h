//
//  SearchViewController.h
//  Nightbike
//
//  Created by Aleksandar Palic on 07.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "ViewController.h"

@protocol SearchViewControllerDelegate <NSObject>

- (void)didDismissSearchViewController:(NSDictionary *)dictionary;

@end

@interface SearchViewController : ViewController <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) id delegate;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *searchTableView;

@property (nonatomic, strong) NSMutableArray *results;

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSDictionary *location;

@property (nonatomic) BOOL isStartingLocation;

@property (nonatomic, strong) NSString *futureSearchBarText;

@end
