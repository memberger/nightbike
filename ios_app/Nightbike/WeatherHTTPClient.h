//
//  WeatherHTTPClient.h
//  bicycleBeam
//
//  Created by Michael Emberger on 21.04.14.
//  Copyright (c) 2014 Michael. All rights reserved.
//

#import "AFHTTPSessionManager.h"
#import <CoreLocation/CoreLocation.h>
#import "Weather.h"

@protocol WeatherHTTPClientDelegate;




@interface WeatherHTTPClient : AFHTTPSessionManager

@property (nonatomic, weak) id<WeatherHTTPClientDelegate>delegate;
@property (strong,nonatomic) NSDate *lastWeatherDataTime;

+ (WeatherHTTPClient *)sharedWeatherHTTPClient;
- (instancetype)initWithBaseURL:(NSURL *)url;
- (void)getWeatherForecastWithLocation: (CLLocation *)location;
- (void)getWeatherForcastWithLocation:(CLLocation *)location andIntervalTime:(int)minutes;

@end

@protocol WeatherHTTPClientDelegate <NSObject>
@optional
- (void)weatherHTTPClient:(WeatherHTTPClient *)client didRequestWeather:(Weather *)weather forLocation:(CLLocation*)location;
- (void)weatherHTTPClient:(WeatherHTTPClient *)client didRequestWeatherWithCondition:(WeatherCondition)condition forLocation:(CLLocation *)location;
@end
