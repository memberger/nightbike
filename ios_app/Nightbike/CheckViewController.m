//
//  CheckViewController.m
//  Nightbike
//
//  Created by Aleksandar Palic on 07.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "CheckViewController.h"
#import <CoreLocation/CLLocationManager.h>


@interface CheckViewController ()

@end

@implementation CheckViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // check for gps
    if ([CLLocationManager locationServicesEnabled]) {
        [[self gpsImage] setImage:[UIImage imageNamed:@"check"]];
        [[self gpsLabel] setText:@"GPS aktiviert"];
    }
    else {
        [[self gpsImage] setImage:[UIImage imageNamed:@"failed"]];
        [[self gpsLabel] setText:@"GPS deaktiviert"];
        
        [[self routeButton] setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [[self routeButton] setEnabled:NO];
    }
    
    [self.ipTextField becomeFirstResponder];
    [self checkConnection];
    
}

- (void)checkConnection
{
    [[self beamerImage] setImage:[UIImage imageNamed:@"failed"]];
    [[self beamerLabel] setText:@"Beamer nicht verbunden"];
    
    OSCCommuicator *communicator = [OSCCommuicator sharedCommunicator];
    communicator.delegate = self;
    [communicator sendConnectMessage];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)unwindToCheckViewController:(UIStoryboardSegue *)segue
{
    //NSLog(@"da");
    [self checkConnection];
}

#pragma mark OSCComminicatorDelegate - Methodes
- (void)didConnectToBeamer:(NSString*)host
{
    //NSLog(@"%@",host);
    self.beamerImage.image = [UIImage imageNamed:@"check"];
    self.beamerLabel.text  = @"Beamer verbunden";
}
- (IBAction)test:(id)sender {
    OSCCommuicator *communicator = [OSCCommuicator sharedCommunicator];
    [communicator sendNavigationMessageWithDirection:DIRECTION_KEEP_STRAIGHT andDistance:@50];
}

@end
