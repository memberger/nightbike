//
//  DailyModeViewController.h
//  nightbike
//
//  Created by Michael on 26.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "OSCCommuicator.h"
#import "WeatherHTTPClient.h"
#import "Weather.h"
#import "Speed.h"
#import "Direction.h"

@interface DailyModeViewController : ViewController<CLLocationManagerDelegate, WeatherHTTPClientDelegate>

@property(strong, nonatomic)Weather *weather;
@property(strong, nonatomic)Speed *speed;
@property(strong, nonatomic)Direction *direction;

@end
