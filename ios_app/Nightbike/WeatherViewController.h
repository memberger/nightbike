//
//  WeatherViewController.h
//  nightbike
//
//  Created by Michael on 26.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "WeatherHTTPClient.h"
#import "Weather.h"
#import "OSCCommuicator.h"

@interface WeatherViewController : ViewController <CLLocationManagerDelegate, WeatherHTTPClientDelegate>

@end
