//
//  DailyModeViewController.m
//  nightbike
//
//  Created by Michael on 26.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "DailyModeViewController.h"

@interface DailyModeViewController ()

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) WeatherHTTPClient *weatherClient;
@property (strong, nonatomic) OSCCommuicator *communicator;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSArray *beamerViews;
@property (assign, nonatomic) NSInteger currentBeamerViewIndex;
@property (strong, nonatomic) NSString *currentView;
@property (assign, nonatomic) BOOL viewChanged;
@property (strong, nonatomic) NSNumber  *currentDistance;
@property (assign, nonatomic) NSInteger currentDirection;

@property (assign, nonatomic) NSInteger loopDirectionIndex;
@property (assign, nonatomic) NSInteger loopWeatherIndex;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (nonatomic) NSString *lastView;

@end

@implementation DailyModeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    
    self.weatherClient.delegate = self;
    
    
//    if(!self.timer)
//    {
//        
//        self.timer = [NSTimer scheduledTimerWithTimeInterval:10.0f
//                                                      target:self
//                                                    selector:@selector(changeBeamerView)
//                                                    userInfo:nil
//                                                     repeats:YES];
//    }
    
}

#pragma mark - Lazy Instanziation
- (CLLocationManager *)locationManager
{
    if(!_locationManager)
    {
        _locationManager = [[CLLocationManager alloc] init];
    }
    return _locationManager;
}

- (WeatherHTTPClient *)weatherClient
{
    if(!_weatherClient)
    {
        _weatherClient = [WeatherHTTPClient sharedWeatherHTTPClient];
    }
    return _weatherClient;
}

- (OSCCommuicator *)communicator
{
    if(!_communicator)
    {
        _communicator = [OSCCommuicator sharedCommunicator];
    }
    return _communicator;
}

- (Weather *)weather
{
    if(!_weather)
    {
        _weather = [[Weather alloc] init];
    }
    return _weather;
}

- (Speed *)speed
{
    if(!_speed)
    {
        _speed = [[Speed alloc] init];
    }
    return _speed;
}

- (Direction *)direction
{
    if(!_direction)
    {
        _direction = [[Direction alloc] init];
    }
    return _direction;
}

- (NSArray *)beamerViews
{
    if(!_beamerViews)
    {
        _beamerViews = @[@"SPEED_VIEW",@"WEATHER_VIEW"];
    }
    return _beamerViews;
}

#pragma mark - Timer selector

- (void)changeBeamerView
{
    
    self.speed.hasChanged = YES;
    self.weather.hasChanged = YES;
    self.direction.hasChanged = YES;
    
    if(!self.currentBeamerViewIndex)
    {
        self.currentBeamerViewIndex = 0;
    }
    
    NSString *nextBeamerView = [self.beamerViews objectAtIndex:self.currentBeamerViewIndex];
    self.currentView = nextBeamerView;
    
    self.currentBeamerViewIndex++;
    if(self.currentBeamerViewIndex == self.beamerViews.count)
    {
        self.currentBeamerViewIndex = 0;
    }
    
    //only for testing direction
    if([self.currentView isEqualToString:@"DIRECTION_VIEW"])
    {
        self.direction.direction = self.loopDirectionIndex;
        
        self.loopDirectionIndex++;
        if(self.loopDirectionIndex == 5)
        {
            self.loopDirectionIndex = 0;
        }
    }
    
    //only for testing weather conditions
    if([self.currentView isEqualToString:@"WEATHER_VIEW"])
    {
        NSLog(@"-----> %d", self.loopWeatherIndex);
        
        self.weather.condition = self.loopWeatherIndex;
        self.loopWeatherIndex++;
        
        if(self.loopWeatherIndex == 11)
        {
            NSLog(@"Drinnen!!");
            self.loopWeatherIndex = 0;
        }
    }
    
    
    [self sendDataToBeamer];
    
    //NSLog(@"change view to: %@", self.currentView);
}


#pragma mark - CLLocation Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = [locations lastObject];
//    NSNumber *speedInKilometersPerHour = [NSNumber numberWithFloat:currentLocation.speed*3.6];

    
    
//    if([speedInKilometersPerHour integerValue] != self.speed.speed)
//    {
//        self.speed.hasChanged = YES;
//        self.speed.speed = [speedInKilometersPerHour integerValue];
//    }
    
    [self.weatherClient getWeatherForcastWithLocation:currentLocation andIntervalTime:1];
    [self sendDataToBeamer];
}

- (void)sendDataToBeamer
{
    NSString *text = self.textView.text;
    
    //NSLog(@"speed has changed %i", self.speed.hasChanged);
    //NSLog(@"weather has changed %i", self.weather.hasChanged);
    //NSLog(@"direction has changed %i", self.direction.hasChanged);
    
    if([self.currentView isEqualToString:@"SPEED_VIEW"])
    {
        //NSLog(@"speed view");
        if(self.speed.hasChanged)
        {

            
            NSLog(@"sent speed data with speed %i", self.speed.speed);
            self.textView.text = [text stringByAppendingString:
                                  [NSString stringWithFormat:@"%@: sent speed data with speed %i\r",[self getCurrentDateAsString], self.speed.speed]];
            [self updateLoggingTextView];
            
            self.speed.hasChanged = NO;
            [self.communicator sendSpeedMessageWithSpeed:self.speed.speed];
            
        }
        
    }
    else if([self.currentView isEqualToString:@"WEATHER_VIEW"])
    {
        //NSLog(@"weather view");
        if(self.weather.hasChanged)
        {
            NSLog(@"sent weather data with weather: %i", self.weather.condition);
            self.textView.text = [text stringByAppendingString:
                                  [NSString stringWithFormat:@"%@: sent weather data with weather: %i\r",[self getCurrentDateAsString], self.weather.condition]];
            [self updateLoggingTextView];
            
            self.weather.hasChanged = NO;
            [self.communicator sendWeatherCondition:self.weather.condition];

        }
    }
    else if([self.currentView isEqualToString:@"DIRECTION_VIEW"])
    {
        //NSLog(@"direction view");
        if(self.direction.hasChanged)
        {
            NSLog(@"sent direction with direction: %i and distance %@", self.direction.direction, self.direction.distance);
            self.textView.text = [text stringByAppendingString:
                                  [NSString stringWithFormat:@"%@: sent direction with direction: %i and distance %@\r",[self getCurrentDateAsString], self.direction.direction, self.direction.distance]];
            [self updateLoggingTextView];
            
            self.direction.hasChanged = NO;
            int distance = arc4random() % 110;
            [self.communicator sendNavigationMessageWithDirection:self.direction.direction andDistance:[NSNumber numberWithInt:distance]];
        }
    }
}

#pragma mark - Weather Delegate

- (void)weatherHTTPClient:(WeatherHTTPClient *)client didRequestWeatherWithCondition:(WeatherCondition)condition forLocation:(CLLocation *)location
{
    self.textView.text = [self.textView.text stringByAppendingString:
                          [NSString stringWithFormat:@"%@: current weather condition: %d\r",[self getCurrentDateAsString], self.weather.condition]];
    [self updateLoggingTextView];


    
    
    if(self.weather.condition != condition)
    {
        self.weather.hasChanged = YES;
        self.weather.condition = condition;
    }

    //NSLog(@"got weather %i", self.weather.condition);
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.timer invalidate];
    self.timer = nil;
}


- (IBAction)directionLeftTouchUpInside:(UIButton *)sender {
    self.currentDirection = DIRECTION_LEFT;
    self.currentDistance = @100;
    [self updateDirectionView];

}
- (IBAction)directionStraightTouchUpInside:(UIButton *)sender {
    self.currentDirection = DIRECTION_KEEP_STRAIGHT;
    self.currentDistance = @100;
    [self updateDirectionView];
}
- (IBAction)directionRightTouchUpInside:(UIButton *)sender {
    self.currentDirection = DIRECTION_RIGHT;
    self.currentDistance = @100;
    [self updateDirectionView];
}
- (IBAction)directionHalfRightTouchUpInside:(UIButton *)sender {
    self.currentDirection = DIRECTION_HALF_RIGHT;
    self.currentDistance = @100;
    [self updateDirectionView];
}
- (IBAction)directionHalfLeftTouchUpInside:(UIButton *)sender {
    self.currentDirection = DIRECTION_HALF_LEFT;
    self.currentDistance = @100;
    [self updateDirectionView];
}

- (IBAction)minusTenTouchUpInside:(UIButton *)sender {
    if([self.currentDistance intValue] > 10)
    {
        self.currentDistance = [NSNumber numberWithInt:[self.currentDistance intValue]-10];
        [self updateDirectionView];
    }
    
}

- (IBAction)speedPlusTouchUpInside:(UIButton *)sender {
    
    if([self.currentView isEqualToString:@"SPEED_VIEW"])
    {
        self.speed.speed++;
        self.speed.hasChanged = YES;
        
        [self sendDataToBeamer];
    }
    else {
        self.currentView = @"SPEED_VIEW";
        self.speed.hasChanged = YES;
        [self sendDataToBeamer];
    }
}


- (IBAction)speedMinusTouchUpInside:(UIButton *)sender {
    
    if([self.currentView isEqualToString:@"SPEED_VIEW"])
    {
        self.speed.speed--;
        self.speed.hasChanged = YES;
        
        [self sendDataToBeamer];
    }
    else {
        self.currentView = @"SPEED_VIEW";
        self.speed.hasChanged = YES;
        [self sendDataToBeamer];
    }
}

- (IBAction)weatherTouchUpInside:(UIButton *)sender {
    self.currentView = @"WEATHER_VIEW";
    
    self.weather.condition = self.loopWeatherIndex;
    self.loopWeatherIndex++;
    
    if(self.loopWeatherIndex == 11)
    {
        self.loopWeatherIndex = 0;
    }
    
    self.weather.hasChanged = YES;
    [self sendDataToBeamer];
}

- (void)updateLoggingTextView {
//    CGPoint bottomOffset = CGPointMake(0, self.textView.contentSize.height - self.textView.bounds.size.height);
//    [self.textView setContentOffset:bottomOffset animated:YES];
    
//    [self.textView scrollRectToVisible:CGRectMake(self.textView.contentSize.width - 1,self.textView.contentSize.height - 1, 1, 1) animated:YES];
//    [self.textView setContentOffset:CGPointMake(0, CGFLOAT_MAX) animated:NO];
    
//    [self.textView setContentOffset:CGPointMake(0.0, self.textView.contentSize.height) animated:YES];
    
    [self.textView scrollRangeToVisible:NSMakeRange([self.textView.text length], 0)];
    [self.textView setScrollEnabled:NO];
    [self.textView setScrollEnabled:YES];
    
}

- (void)updateDirectionView
{
    self.textView.text = [self.textView.text stringByAppendingString:
                          [NSString stringWithFormat:@"%@: sent direction with direction: %i and distance %@\r",[self getCurrentDateAsString], self.currentDirection, self.currentDistance]];
    [self updateLoggingTextView];
    
    [self.communicator sendNavigationMessageWithDirection:self.currentDirection andDistance:self.currentDistance];
    
    // [self restartTimer];
}


- (void)restartTimer
{
    [self.timer invalidate];
    self.timer = nil;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10.0f
                                                  target:self
                                                selector:@selector(changeBeamerView)
                                                userInfo:nil
                                                 repeats:YES];
    self.currentBeamerViewIndex = 0;
    
}

-(NSString *)getCurrentDateAsString
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formater = [[NSDateFormatter alloc]init];
    [formater setTimeStyle:NSDateFormatterMediumStyle];
    NSString *dateString = [formater stringFromDate:date];
    
    return dateString;
}



@end