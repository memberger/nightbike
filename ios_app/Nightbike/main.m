//
//  main.m
//  Nightbike
//
//  Created by Aleksandar Palic on 07.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
