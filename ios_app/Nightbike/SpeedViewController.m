//
//  SpeedViewController.m
//  Nightbike
//
//  Created by Michael Emberger on 15.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "SpeedViewController.h"


@interface SpeedViewController ()
@property (weak, nonatomic) IBOutlet UILabel *speedLabel;

@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation SpeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    self.speedLabel.text = @"---";
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Lazy Instanziation
- (CLLocationManager *)locationManager
{
    if(!_locationManager)
    {
        _locationManager = [[CLLocationManager alloc] init];
    }
    return _locationManager;
}

#pragma mark - CLLocation Delegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Location retrieval did fail with error: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = [locations lastObject];
    float speedInKilometersPerHour = currentLocation.speed*3.6;
    
    
    
    if(speedInKilometersPerHour >= 0)
    {
        self.speedLabel.text = [NSString stringWithFormat:@"%.2f", speedInKilometersPerHour];
        OSCCommuicator *communicator = [OSCCommuicator sharedCommunicator];
        [communicator sendSpeedMessageWithSpeed:(int)speedInKilometersPerHour];
    }
    else
    {
        self.speedLabel.text = @"?";
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
