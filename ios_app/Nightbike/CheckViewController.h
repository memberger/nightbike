//
//  CheckViewController.h
//  Nightbike
//
//  Created by Aleksandar Palic on 07.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "ViewController.h"
#import "OSCCommuicator.h"

@interface CheckViewController : ViewController <OSCCommunicatorDelegate>

@property (weak, nonatomic) IBOutlet UILabel *gpsLabel;
@property (weak, nonatomic) IBOutlet UILabel *beamerLabel;

@property (weak, nonatomic) IBOutlet UIImageView *gpsImage;
@property (weak, nonatomic) IBOutlet UIImageView *beamerImage;

@property (weak, nonatomic) IBOutlet UIButton *routeButton;

@property (weak, nonatomic) IBOutlet UITextField *ipTextField;
@end
