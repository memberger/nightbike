//
//  SearchViewController.m
//  Nightbike
//
//  Created by Aleksandar Palic on 07.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "RouteViewController.h"
#import "SearchViewController.h"
#import "AFHTTPRequestOperationManager.h"

@interface SearchViewController ()

@property (nonatomic, strong) const NSString *apiKey;

@end

@implementation SearchViewController {
    NSTimer *searchDelayer;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setApiKey:@"AIzaSyCbwxinDbmCv-HgvvJudgDmJxwK1IdIxsk"];
    
    [self setResults:[[NSMutableArray alloc] init]];
    
    [[self searchBar] setDelegate:self];
    [[self searchBar] becomeFirstResponder];
    
    [[self searchBar] setText:[self futureSearchBarText]];
    
    [[self searchTableView] setDelegate:self];
    [[self searchTableView] setDataSource:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{    
    [[self delegate] didDismissSearchViewController:@{
        @"address": [self address],
        @"location": [self location],
        @"isStartingLocation": [NSNumber numberWithBool:[self isStartingLocation]]
    }];
}

#pragma mark Searchbar Delegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [searchDelayer invalidate], searchDelayer = nil;
    
    searchDelayer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                     target:self
                                                   selector:@selector(doDelayedSearch:)
                                                   userInfo:searchText
                                                    repeats:NO];
}

- (void)doDelayedSearch:(NSTimer *)t
{
    assert(t == searchDelayer);
    searchDelayer = nil;
    
    NSString *searchBarText = [[[self searchBar] text] stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *URLString = @"https://maps.googleapis.com/maps/api/place/autocomplete/json";
    
    NSDictionary *parameters = @{
        @"input": searchBarText,
        @"sensor": @true,
        @"types": @"geocode",
        @"key": [self apiKey]
    };
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:URLString parameters:parameters error:nil];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [[self results] removeAllObjects];
                
        for (NSDictionary *street in [responseObject valueForKey:@"predictions"]) {
            [[self results] addObject:street];
        }
        
        [[self searchTableView] reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
    [operation start];
}

#pragma mark TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self results] count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reference = [[[self results] objectAtIndex:[indexPath row]] valueForKey:@"reference"];
    NSString *googleUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&sensor=true&key=%@", reference, [self apiKey]];
    
    NSArray *terms = [[[self results] objectAtIndex:[indexPath row]] valueForKey:@"terms"];
    NSString *address = [[terms objectAtIndex:0] valueForKey:@"value"];
    NSString *place = @"Empty";
    NSString *country = @"Empty";
    
    if ([terms count] > 1) {
        place = [[terms objectAtIndex:1] valueForKey:@"value"];
    }
    if ([terms count] > 2) {
        country = [[terms objectAtIndex:2] valueForKey:@"value"];
    }
    
    [self setAddress:[NSString stringWithFormat:@"%@, %@, %@", address, place, country]];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:googleUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *location = [[[(NSDictionary *)responseObject valueForKey:@"result"] valueForKey:@"geometry"] valueForKey:@"location"];
        
        [self setAddress:address];
        [self setLocation:location];
        
        //[self performSegueWithIdentifier:@"backFromSearchViewController" sender:self];
        [self dismissModalViewControllerAnimated:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *row = [[self results] objectAtIndex:[indexPath row]];

    NSArray *terms = [row valueForKey:@"terms"];
    NSString *address = [[terms objectAtIndex:0] valueForKey:@"value"];
    NSString *place = @"Empty";
    NSString *country = @"Empty";
    
    if ([terms count] > 1) {
        place = [[terms objectAtIndex:1] valueForKey:@"value"];
    }
    if ([terms count] > 2) {
        country = [[terms objectAtIndex:2] valueForKey:@"value"];
    }
    
    UITableViewCell *cell = [[self searchTableView] dequeueReusableCellWithIdentifier:@"streetCell"];
    [[cell textLabel] setText:address];
    [[cell detailTextLabel] setText:[NSString stringWithFormat:@"%@, %@", place, country]];
    
    return cell;
}

@end
