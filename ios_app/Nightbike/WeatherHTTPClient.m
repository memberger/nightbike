//
//  WeatherHTTPClient.m
//  bicycleBeam
//
//  Created by Michael Emberger on 21.04.14.
//  Copyright (c) 2014 Michael. All rights reserved.
//

#import "WeatherHTTPClient.h"

@interface WeatherHTTPClient()

@property BOOL weatherCatched;

@end

@implementation WeatherHTTPClient

static NSString * const WeatherBaseUrl = @"http://api.openweathermap.org";

+ (WeatherHTTPClient *)sharedWeatherHTTPClient
{
    static WeatherHTTPClient *_sharedWeatherHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedWeatherHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:WeatherBaseUrl]];
    });
    
    return _sharedWeatherHTTPClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        self.requestSerializer  = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

-(NSDate *)lastWeatherDataTime
{
    if(!_lastWeatherDataTime)
    {
        _lastWeatherDataTime = [NSDate date];
    }
    return _lastWeatherDataTime;
}

- (void)getWeatherForecastWithLocation:(CLLocation *)location
{

    [self GET:@"/data/2.5/forecast/daily" parameters:@{
                                              @"lat": [NSString stringWithFormat:@"%f",location.coordinate.latitude],
                                              @"lon": [NSString stringWithFormat:@"%f",location.coordinate.longitude],
                                              @"cnt": @"1",
                                              @"units":@"metric",
                                              @"mode": @"json"}
      success:^(NSURLSessionDataTask *task, id responseObject) {
          
        self.lastWeatherDataTime = [NSDate date];
          
          Weather *weather = [self parseWeatherData:responseObject];
          //NSLog(@"%i",weather.condition);
          
          [self.delegate weatherHTTPClient:self didRequestWeatherWithCondition:weather.condition forLocation:location];
          
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)getWeatherForcastWithLocation:(CLLocation *)location andIntervalTime:(int)minutes
{
    float difference = [[NSDate date] timeIntervalSinceDate:self.lastWeatherDataTime];
    float intervalTime = minutes * 60;
    
    //NSLog(@"%f",difference);
    //NSLog(@"%f",intervalTime);
    
    if(difference > intervalTime)
    {
        [self getWeatherForecastWithLocation:location];
    }
    else if(!self.weatherCatched)
    {
        [self getWeatherForecastWithLocation:location];
        self.weatherCatched = YES;
    }
}

- (Weather *)parseWeatherData:(NSDictionary *)responseObject
{
    NSDictionary *forcast = [responseObject valueForKey:@"list"];
    NSDictionary *forcastWeather = [[forcast valueForKey:@"weather"] objectAtIndex:0];
    NSInteger condID = [[[forcastWeather valueForKey:@"id"] objectAtIndex:0] intValue];
    
    NSInteger condition;
    
    if(condID >= 200 && condID < 300)
    {
        condition = WC_THUNDERSTORM;
    }
    else if(condID == 301)
    {
        condition = WC_DRIZZLE;
    }
    else if(condID == 300 || (condID > 301 && condID < 400))
    {
        condition = WC_DRIZZLE_STRONG;
    }
    else if(condID >=500 && condID <= 501)
    {
        condition = WC_RAIN;
    }
    else if(condID >= 502 && condID < 600)
    {
        condition = WC_RAIN_STRONG;
    }
    else if(condID >= 600 && condID < 700)
    {
        condition = WC_SNOW;
    }
    else if(condID == 741)
    {
        condition = WC_FOG;
    }
    else if(condID >= 800 && condID < 900)
    {
        condition = WC_CLOUD;
    }
    else if(condID == 905)
    {
        condition = WC_EXTREME_WIND;
    }
    else if(condID == 906)
    {
        condition = WC_EXTREME_HAIL;
    }
    else
    {
        condition = WC_NOT_AVAILABLE;
    }
    
    Weather *weather = [[Weather alloc] init];
    weather.condition = condition;
    
    return weather;
}



@end
