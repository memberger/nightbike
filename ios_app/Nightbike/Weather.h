//
//  Weather.h
//  nightbike
//
//  Created by Michael on 26.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, WeatherCondition) {
    WC_NOT_AVAILABLE,
    WC_THUNDERSTORM,
    WC_DRIZZLE,
    WC_DRIZZLE_STRONG,
    WC_RAIN,
    WC_RAIN_STRONG,
    WC_SNOW,
    WC_FOG,
    WC_CLOUD,
    WC_EXTREME_HAIL,
    WC_EXTREME_WIND
};

@interface Weather : NSObject

@property(assign, nonatomic)NSInteger condition;
@property(nonatomic)BOOL hasChanged;

@end
