//
//  OSCCommuicator.m
//  bicycleBeam
//
//  Created by Michael on 05.04.14.
//  Copyright (c) 2014 Michael. All rights reserved.
//

#import "OSCCommuicator.h"

@interface OSCCommuicator()



@end


@implementation OSCCommuicator


+ (OSCCommuicator *)sharedCommunicator
{
    static OSCCommuicator *_sharedCommunicator = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedCommunicator = [[self alloc] init];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if([defaults valueForKey:@"receiverHost"])
        {
            _sharedCommunicator.receiverHost = [defaults valueForKey:@"receiverHost"];
            [_sharedCommunicator setupOSCServerAndStartListening];
        }
    });
    
    return _sharedCommunicator;
}

- (F53OSCClient *)oscClient
{
    if(!_oscClient)
    {
        _oscClient = [[F53OSCClient alloc] init];
    }
    return _oscClient;
}

- (NSString *)receiverHost
{
    if(!_receiverHost)
    {
        _receiverHost = RECEIVER_HOST;
    }
    return _receiverHost;
}

- (NSNumber *)receiverPort
{
    if(!_receiverPort)
    {
        _receiverPort = [NSNumber numberWithInt:RECEIVER_PORT];
    }
    return _receiverPort;
}

- (void)sendConnectMessage
{
    self.isConnected = NO;
    F53OSCMessage *message = [F53OSCMessage messageWithAddressPattern:@"/connect" arguments:@[self.receiverHost]];
    [self.oscClient sendPacket:message toHost:self.receiverHost onPort:[self.receiverPort intValue]];
}

- (void)setupOSCServerAndStartListening
{
    self.oscServer = [[F53OSCServer alloc] init];
    self.oscServer.port = 12345;
    self.oscServer.delegate = self;
    [self.oscServer startListening];
    
}



- (void)saveReceiverHost:receiverHost
{
    self.receiverHost = receiverHost;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:receiverHost forKey:@"receiverHost"];
}

- (void)sendSpeedMessageWithSpeed:(NSInteger)speed
{
    F53OSCMessage *message = [F53OSCMessage messageWithAddressPattern:@"/speed" arguments:@[[NSNumber numberWithInt:speed]]];
    [self.oscClient sendPacket:message toHost:self.receiverHost onPort:[self.receiverPort intValue]];
}

- (void)sendNavigationMessageWithDirection:(Directions)direction andDistance:(NSNumber*)distance
{
    F53OSCMessage *message = [F53OSCMessage messageWithAddressPattern:@"/direction" arguments:@[[NSNumber numberWithInt:direction],distance]];
    [self.oscClient sendPacket:message toHost:self.receiverHost onPort:[self.receiverPort intValue]];
}

- (void)sendWeatherCondition:(WeatherCondition)condition
{
    F53OSCMessage *message = [F53OSCMessage messageWithAddressPattern:@"/weather_condition" arguments:@[[NSNumber numberWithInt:condition]]];
    [self.oscClient sendPacket:message toHost:self.receiverHost onPort:[self.receiverPort intValue]];
}


#pragma mark F53OSCPacketDestination Methodes

-(void) takeMessage:(F53OSCMessage *)message
{
    if([message.addressPattern isEqualToString:@"/connect"])
    {
        self.isConnected = YES;
        //NSLog(@"%@", message.arguments);
        if(message.arguments.count > 0)
        {
            [self.delegate didConnectToBeamer:(NSString *)[message.arguments objectAtIndex:0]];
        }
        
    }
   
}

@end
