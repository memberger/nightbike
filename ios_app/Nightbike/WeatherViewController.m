//
//  WeatherViewController.m
//  nightbike
//
//  Created by Michael on 26.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "WeatherViewController.h"


@interface WeatherViewController ()

@property (strong, nonatomic) CLLocationManager *locationManager;


@end

@implementation WeatherViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
    
    // Do any additional setup after loading the view.
}

- (CLLocationManager *)locationManager
{
    if(!_locationManager)
    {
        _locationManager = [[CLLocationManager alloc]init];
    }
    return _locationManager;
}


#pragma mark - CoreLocationDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"Location retrieval did fail with error: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *currentLocation = [locations lastObject];
 
    WeatherHTTPClient *weatherClient = [WeatherHTTPClient sharedWeatherHTTPClient];
    weatherClient.delegate = self;
    [weatherClient getWeatherForcastWithLocation:currentLocation andIntervalTime:1];
    
    
    
}

#pragma mark - Weather Delegate

-(void)weatherHTTPClient:(WeatherHTTPClient *)client didRequestWeatherWithCondition:(WeatherCondition)condition forLocation:(CLLocation *)location
{
    //NSLog(@"here the delegate conditions %i", condition);
    OSCCommuicator *communicator = [OSCCommuicator sharedCommunicator];
    [communicator sendWeatherCondition:condition];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

