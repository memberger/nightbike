//
//  OSCCommuicator.h
//  bicycleBeam
//
//  Created by Michael on 05.04.14.
//  Copyright (c) 2014 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "F53OSC.h"
#import "Weather.h"
#import "Direction.h"

#define RECEIVER_HOST @"172.20.10.8"
#define RECEIVER_PORT 9000

@class OSCCommuicator;

@protocol OSCCommunicatorDelegate <NSObject>

@optional
-(void)didConnectToBeamer:(NSString *)host;

@end


@interface OSCCommuicator : NSObject <F53OSCPacketDestination>

@property(weak, nonatomic)id <OSCCommunicatorDelegate> delegate;
@property(strong, nonatomic) F53OSCClient *oscClient;
@property(strong, nonatomic) F53OSCServer *oscServer;
@property(strong, nonatomic) NSString *receiverHost;
@property(strong, nonatomic) NSNumber *receiverPort;
@property(assign, nonatomic) BOOL isConnected;



+ (OSCCommuicator *)sharedCommunicator;
- (void)saveReceiverHost:(NSString *)receiverHost;
- (void)sendConnectMessage;
- (void)sendSpeedMessageWithSpeed:(NSInteger)speed;
- (void)sendNavigationMessageWithDirection:(Directions)direction andDistance:(NSNumber*)distance;
- (void)sendWeatherCondition:(WeatherCondition)condition;


@end

