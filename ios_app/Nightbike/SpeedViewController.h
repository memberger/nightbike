//
//  SpeedViewController.h
//  Nightbike
//
//  Created by Michael Emberger on 15.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "ViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "OSCCommuicator.h"

@interface SpeedViewController : ViewController<CLLocationManagerDelegate>

@end
