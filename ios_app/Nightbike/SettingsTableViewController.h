//
//  SettingsTableViewController.h
//  Nightbike
//
//  Created by Michael on 18.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OSCCommuicator.h"

@interface SettingsTableViewController : UITableViewController <OSCCommunicatorDelegate>

@end
