//
//  Speed.h
//  nightbike
//
//  Created by Michael on 29.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Speed : NSObject
@property(assign,nonatomic)NSInteger speed;
@property(nonatomic)BOOL hasChanged;
@end
