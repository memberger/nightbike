//
//  Direction.h
//  nightbike
//
//  Created by Michael on 29.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, Directions) {
    DIRECTION_LEFT,
    DIRECTION_RIGHT,
    DIRECTION_KEEP_STRAIGHT,
    DIRECTION_HALF_RIGHT,
    DIRECTION_HALF_LEFT
};

@interface Direction : NSObject
@property(assign,nonatomic)Directions direction;
@property(strong,nonatomic)NSNumber *distance;
@property(nonatomic)BOOL hasChanged;
@end
