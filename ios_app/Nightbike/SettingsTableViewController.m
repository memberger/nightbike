//
//  SettingsTableViewController.m
//  Nightbike
//
//  Created by Michael on 18.05.14.
//  Copyright (c) 2014 Aleksandar Palic. All rights reserved.
//

#import "SettingsTableViewController.h"


@interface SettingsTableViewController ()
@property (weak, nonatomic) IBOutlet UITextField *hostLabel;
@property (strong, nonatomic) NSString *currentIp;
@property (assign, nonatomic) NSInteger ipIndex;
@property (strong, nonatomic)NSTimer *timer;

@end

@implementation SettingsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    OSCCommuicator *communicator = [OSCCommuicator sharedCommunicator];
    if(communicator.receiverHost)
    {
        self.hostLabel.text = communicator.receiverHost;
    }
    
    communicator.delegate = self;
    
//    self.tableView.backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 800)];
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    bgImageView.image = [UIImage imageNamed:@"bg"];
    
    self.tableView.backgroundView = bgImageView;
    [self.tableView sendSubviewToBack:self.tableView.backgroundView];
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveButtonTouched:(id)sender {
    OSCCommuicator *communicator = [OSCCommuicator sharedCommunicator];
    [communicator saveReceiverHost:self.hostLabel.text];
    
    [self performSegueWithIdentifier:@"unwindToCheckViewController" sender:self];
}

- (IBAction)searchButtonTouchUpInside:(UIButton *)sender {
    
    if(!self.timer)
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1f
                                                      target:self
                                                    selector:@selector(checkIp)
                                                    userInfo:nil
                                                     repeats:YES];
    }
   
}

-(void)checkIp
{
    
    OSCCommuicator *communicator = [OSCCommuicator sharedCommunicator];
    communicator.receiverHost = [NSString stringWithFormat:@"172.20.10.%i", self.ipIndex];
    //NSLog(@"%@", communicator.receiverHost);
    [communicator sendConnectMessage];
    
    if (self.ipIndex < 255)
    {
        self.ipIndex++;
    }
    else
    {
        [self.timer invalidate];
        self.timer = nil;
    }
}

-(void)didConnectToBeamer:(NSString*)host
{
    //NSLog(@"adress found: %@",host);
    self.hostLabel.text = host;
    [self.timer invalidate];
    self.timer = nil;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.timer invalidate];
    self.timer = nil;
}

@end
