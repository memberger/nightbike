#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
    ofSetVerticalSync(true);
    
    ofBackground(0);
    ofSetColor(255);
    
    //Setup Font
    ofTrueTypeFont::setGlobalDpi(72);
    verdanaLarge.loadFont("verdana.ttf", 200, true, true);
    verdanaSmall.loadFont("verdana.ttf", 60, true, true);
    
    img_direction_left.loadImage("direction_left.png");
    img_direction_right.loadImage("direction_right.png");
    img_direction_keep_straight.loadImage("direction_keep_straight.png");
    img_direction_half_right.loadImage("direction_half_right.png");
    img_direction_half_left.loadImage("direction_half_left.png");
    

    
    //Setup OSC
    // Enable some logging information
	ofSetLogLevel(OF_LOG_VERBOSE);
    
	//Server side
	//listen for incoming messages on a port; setup OSC receiver with usage:
	serverRecvPort = 9000;
	serverReceiver.setup(serverRecvPort);
    

    
    clientDestination = "10.0.1.2";
    clientSendPort = 12345;
    clientSender.setup(clientDestination, clientSendPort);
    
	clientRecvPort = clientSendPort + 1;
	clientReceiver.setup(clientRecvPort);
    
    adjustX = 0.0f;
    
    currentView = NO_VIEW;
    
    //Images
    //wc_rain.loadImage("wc_rain.png");
    
    
    

}
    
    //--------------------------------------------------------------
    void testApp::update(){
        step += 0.001;
        if (step > 1) {
            step -= 1;
        }
        
        // OSC receiver queues up new messages, so you need to iterate
        // through waiting messages to get each incoming message
        
        // check for waiting messages
        while(serverReceiver.hasWaitingMessages()){
            // get the next message
            ofxOscMessage m;
            serverReceiver.getNextMessage(&m);
            //Log received message for easier debugging of participants' messages:
          //  ofLogVerbose("Server recvd msg " + getOscMsgAsString(m) + " from " + m.getRemoteIp());
            
            // check the address of the incoming message
            
            if(m.getAddress() == "/connect"){
                
              
                //Client side
                clientDestination = m.getRemoteIp();
                clientSendPort = 12345;
                clientSender.setup(clientDestination, clientSendPort);
                
                string incomingHost = m.getRemoteIp();
                string host;
                
                ofLogVerbose("Server recvd msg " + getOscMsgAsString(m) + " from " + m.getRemoteIp());
            
                if(m.getNumArgs() > 0)
                {
                    host = m.getArgAsString(0);
                }
                
                ofxOscMessage sm;
                sm.setAddress("/connect");
                sm.addStringArg(host);
                clientSender.sendMessage(sm);
                
            }
            else if(m.getAddress() == "/speed"){
                currentView = SPEED_VIEW;
                if(m.getNumArgs() > 0)
                {
                    currentView = SPEED_VIEW;
                    currentSpeed.kmh = m.getArgAsInt32(0);
                }
                
            }
            else if(m.getAddress() == "/direction"){
                //Identify host of incoming msg
                string incomingHost = m.getRemoteIp();
                // get the first argument (we're only sending one) as a string
                if(m.getNumArgs() > 0){
                    currentView = NAVIGATION_VIEW;
                    currentDirection.direction = m.getArgAsInt32(0);
                    currentDirection.distance  = m.getArgAsInt32(1);
                    
                    if(m.getArgType(0) == OFXOSC_TYPE_STRING){

                    }
                }
            }
            else if(m.getAddress() == "/weather_condition"){
                //Identify host of incoming msg
                string incomingHost = m.getRemoteIp();
                // get the first argument (we're only sending one) as a string
                if(m.getNumArgs() > 0){
                    currentView = WEATHER_VIEW;
                    currentWeather.condition = m.getArgAsInt32(0);
                    
                    if(m.getArgType(0) == OFXOSC_TYPE_STRING){
                        
                    }
                }
            }
            // handle getting random OSC messages here
            else{
                ofLogWarning("Server got weird message: " + m.getAddress());
            }
        }
        
        //this is purely workaround for a mysterious OSCpack bug on 64bit linux
        // after startup, reinit the receivers
        // must be a timing problem, though - in debug, stepping through, it works.
        if(ofGetFrameNum() == 60){
            clientReceiver.setup(clientRecvPort);
            serverReceiver.setup(serverRecvPort);
        }
        
    }
    
    //--------------------------------------------------------------
    void testApp::draw(){
        
        
        
        adjustX = 100;
        
        float resX = 845;
        float resY = 480;
        ofEnableSmoothing();
        ofSetCircleResolution(100);
        ofSetColor(255);
        ofEllipse(resX/2,resY/2,resY+adjustX,resY);
        
        if(currentView == NO_VIEW)
        {
            string ipLabel;
            float x, y;
            ofRectangle bounds;
            ofSetColor(0,0,0);
            

            ipLabel = "welcome";

            
            bounds = verdanaSmall.getStringBoundingBox(ipLabel, 0, 0);
            x = (resX / 2) - (bounds.width / 2)-5;
            y = (resY / 2) + (bounds.height / 2);
            verdanaSmall.drawString(ipLabel, x, y);
            
        }
        
        
        if(currentView == NAVIGATION_VIEW)
        {
            ofImage image;
            
            switch (currentDirection.direction) {
                case DIRECTION_KEEP_STRAIGHT:
                    image = img_direction_keep_straight;
                    break;
                case DIRECTION_LEFT:
                    image = img_direction_left;
                    break;
                case DIRECTION_RIGHT:
                    image = img_direction_right;
                    break;
                case DIRECTION_HALF_LEFT:
                    image = img_direction_half_left;
                    break;
                case DIRECTION_HALF_RIGHT:
                    image = img_direction_half_right;
                    break;
                default:
                    image = img_direction_keep_straight;
                    break;
            }

            image.resize(250, 250);
            int x = (845 - image.width) / 2;
            int y = (480 - image.height - 50) /2;
            
            image.draw(x,y);
            
            ofSetColor(0);
            
            if(!currentDirection.distance)
            {
                currentDirection.distance = 100;
            }
            int elementWidth = 20;
            int elements = currentDirection.distance / 10;
            
            float middleLeftDistance = ((elements * 2 -1) * elementWidth) / 2;
            float leftMarginFirstElement = (845 / 2) - middleLeftDistance;
            
            for(int i = 0; i < elements; i++)
            {
                float margin = leftMarginFirstElement + (2 * elementWidth * i);
                ofRect(margin, 360, elementWidth,elementWidth);
            }
        }
        
        if(currentView == SPEED_VIEW)
        {
            float x, y;
            ofRectangle bounds;

            ofSetColor(0,0,0);
            string speedLabel = "km/h";
            bounds = verdanaSmall.getStringBoundingBox(speedLabel, 0, 0);
            x = (resX / 2) - (bounds.width / 2)-5;
            y = (resY / 2) + (bounds.height / 2) + 120;
            verdanaSmall.drawString(speedLabel, x, y);
            
            string speed = ofToString(currentSpeed.kmh);
            bounds = verdanaLarge.getStringBoundingBox(speed, 0, 0);
            x = (resX / 2) - (bounds.width / 2)- 25;
            y = (resY / 2) + (bounds.height / 2)- 45;
            verdanaLarge.drawString(speed, x, y);
        }
        
        if(currentView == WEATHER_VIEW)
        {
            ofImage image;
            
            switch (currentWeather.condition) {
                case WC_THUNDERSTORM:
                    image.loadImage("wc_thunderstorm.png");
                    break;
                case WC_DRIZZLE:
                    image.loadImage("wc_drizzle.png");
                    break;
                case WC_DRIZZLE_STRONG:
                    image.loadImage("wc_drizzle_strong.png");
                    break;
                case WC_RAIN:
                    image.loadImage("wc_rain.png");
                    break;
                case WC_RAIN_STRONG:
                    image.loadImage("wc_rain_strong.png");
                    break;
                case WC_SNOW:
                    image.loadImage("wc_snow.png");
                    break;
                case WC_FOG:
                    image.loadImage("wc_fog.png");
                    break;
                case WC_CLOUD:
                    image.loadImage("wc_cloud.png");
                    break;
                case WC_EXTREME_HAIL:
                    image.loadImage("wc_extreme_hail.png");
                    break;
                case WC_EXTREME_WIND:
                    image.loadImage("wc_extreme_wind.png");
                    break;
                case WC_NOT_AVAILABLE:
                    image.loadImage("wc_extreme_hail.png");
                    break;
                default:
                    image.loadImage("wc_extreme_hail.png");
                    break;
            }
            
            int x = (845 - image.width) / 2;
            int y = (480 - image.height) /2;
        
            image.draw(x,y);
        }
        
    }
    


string testApp::getOscMsgAsString(ofxOscMessage m){
	string msg_string;
	msg_string = m.getAddress();
	msg_string += ":";
	for(int i = 0; i < m.getNumArgs(); i++){
		// get the argument type
		msg_string += " " + m.getArgTypeName(i);
		msg_string += ":";
		// display the argument - make sure we get the right type
		if(m.getArgType(i) == OFXOSC_TYPE_INT32){
			msg_string += ofToString(m.getArgAsInt32(i));
		}
		else if(m.getArgType(i) == OFXOSC_TYPE_FLOAT){
			msg_string += ofToString(m.getArgAsFloat(i));
		}
		else if(m.getArgType(i) == OFXOSC_TYPE_STRING){
			msg_string += m.getArgAsString(i);
		}
		else{
			msg_string += "unknown";
		}
	}
	return msg_string;
}
