#include "ofMain.h"
#include "testApp.h"
#include "defines.h"

//========================================================================
int main( ){
	ofSetupOpenGL(845,480,OF_WINDOW);			// <-------- setup the GL context
    ofSetFullscreen(true);
	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:
	ofRunApp(new testApp());

}
