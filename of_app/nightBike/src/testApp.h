#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "defines.h"


class testApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();


    
    ofTrueTypeFont	verdanaLarge;
    ofTrueTypeFont	verdanaSmall;
    float step;
    vector<ofPolyline> outlines;
    
    //ofImage wc_rain;
    
    enum Views
    {
        SPEED_VIEW,
        WEATHER_VIEW,
        NAVIGATION_VIEW,
        NO_VIEW
    };
    
    int currentView;
    
    enum Directions
    {
        DIRECTION_LEFT,
        DIRECTION_RIGHT,
        DIRECTION_KEEP_STRAIGHT,
        DIRECTION_HALF_RIGHT,
        DIRECTION_HALF_LEFT
    };
    
    enum Conditions
    {
        WC_NOT_AVAILABLE,
        WC_THUNDERSTORM,
        WC_DRIZZLE,
        WC_DRIZZLE_STRONG,
        WC_RAIN,
        WC_RAIN_STRONG,
        WC_SNOW,
        WC_FOG,
        WC_CLOUD,
        WC_EXTREME_HAIL,
        WC_EXTREME_WIND
    };
    
    ofImage img_direction_left;
    ofImage img_direction_right;
    ofImage img_direction_keep_straight;
    ofImage img_direction_half_right;
    ofImage img_direction_half_left;
    
    struct speed {
        int kmh;
        int maxkmh;
    } currentSpeed;
    
    struct direction {
        int direction;
        int distance;
    } currentDirection;
    
    struct weather {
        int condition;
    } currentWeather;
    
    float adjustX;
    
    int direction;
    int distance;
        
    //----------------------------------------
    // Client side:
    
    ofxOscSender clientSender; // all-important ofxOscSender object
    string clientDestination; // IP address we're sending to
    int clientSendPort; // port we're sending to
    string clientTyping; // what we're going to send: some stuff you typed
    
    ofxOscReceiver clientReceiver; // OSC receiver
    int clientRecvPort; // port where we listen for messages
    string clientMessages; // string containing the received messages for display
    
    //----------------------------------------
    // Server side:
    ofxOscReceiver serverReceiver; // OSC receiver
    int serverRecvPort; // port we're listening on: must match port from sender!
    string serverTyping; //messages you've received from the clientes
    
    // Parse an OscMessage into a string for easy logging
    string getOscMsgAsString(ofxOscMessage m);
    
    string ipAdress;
    

    
};
